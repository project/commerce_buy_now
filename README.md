CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration



INTRODUCTION
------------

The Commerce Buy Now module provides the ability for customers checkout
directly from the product page with just one click, skipping all other steps.

REQUIREMENTS
------------

This module requires the following modules:

Commerce Core
Commerce Cart
Commerce Store
Commerce Checkout

INSTALLATION
------------

Install as usual, see https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

CONFIGURATION
------------

No configuration is needed.

MAINTAINERS
------------

 * Maksym Fedorchuk - [Maksym Fedorchuk](https://www.drupal.org/u/maksym-fedorchuk)
 * Mykhailo Gurei - [ozin](https://www.drupal.org/u/ozin)

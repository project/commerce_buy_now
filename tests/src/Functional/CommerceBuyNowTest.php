<?php

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;

/**
 * Tests the Commerce Buy Now module.
 *
 * @group commerce_buy_now
 */
class CommerceBuyNowTest extends CommerceBrowserTestBase {

  /**
   * The cart order to test against.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $cart;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_product',
    'commerce_order',
    'commerce_order_test',
    'inline_entity_form',
    'commerce_buy_now',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_product',
      'access content',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();

    $this->variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => $this->randomMachineName(),
      'price' => [
        'number' => 999,
        'currency_code' => 'USD',
      ],
    ]);

    $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation],
    ]);

  }

  /**
   * Tests the "Buy Now" functionality.
   */
  public function testBuyNowFunctionality() {

    /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $order_item_form_display */
    $order_item_form_display = EntityFormDisplay::load('commerce_order_item.default.add_to_cart');
    $order_item_form_display->setComponent('quantity', [
      'type' => 'commerce_quantity',
    ]);
    $order_item_form_display->save();
    $this->drupalGet('product/' . $this->variation->getProduct()->id());
    $this->assertSession()->buttonExists('Buy Now');
    $value = [
      'quantity[0][value]' => '3',
    ];

    $this->submitForm($value, 'Buy Now');
    $this->assertSession()->addressEquals('checkout/1/order_information');
    $label = $this->variation->getProduct()->label();
    $this->assertSession()->pageTextContains($this->t('@label added to your cart.', ['@label' => $label]));
  }

}
